<?php

require_once 'vendor/autoload.php';

use SEStolk\ImagePlaceholder\Color\Color;
use SEStolk\ImagePlaceholder\Generator\ImageOptions;
use SEStolk\ImagePlaceholder\Generator\ImagePlaceholder;
use SEStolk\ImagePlaceholder\Generator\ImageReturnType;

$imageOptions = new ImageOptions();
$imageOptions->setCacheDirectory(__DIR__);
$imageOptions->setCacheEnabled(false);
$imageOptions->setBackgroundColor(new Color(255, 255, 255));
$imageOptions->setTextColor(new Color(0, 0, 0));
$imageOptions->setTextShadowColor(new Color(128, 128, 128));
$imageOptions->setWidth(400);
$imageOptions->setHeight(400);

$image = new ImagePlaceholder($imageOptions);
$image->setReturnType(ImageReturnType::RETURN_TYPE_PATH);
echo $image->generate('Joey');