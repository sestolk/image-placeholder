<?php namespace SEStolk\ImagePlaceholder;

use Illuminate\Support\ServiceProvider;
use SEStolk\ImagePlaceholder\Color\Color;
use SEStolk\ImagePlaceholder\Generator\ImageOptions;
use SEStolk\ImagePlaceholder\Interfaces\Color\ColorInterface;
use SEStolk\ImagePlaceholder\Interfaces\ImageOptionsInterface;

class ImagePlaceholderServiceProvider extends ServiceProvider {
	public function register() {
		$this->app->bind(ImageOptionsInterface::class, ImageOptions::class);
		$this->app->bind(ColorInterface::class, Color::class);
	}

	public function provides() {
		return ['ImagePlaceholder'];
	}
}
