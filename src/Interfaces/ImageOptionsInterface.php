<?php namespace SEStolk\ImagePlaceholder\Interfaces;

use SEStolk\ImagePlaceholder\Interfaces\Color\ColorInterface;

interface ImageOptionsInterface {
	public function setHeight(int $height): self;
	public function getHeight(): int;

	public function setWidth(int $width): self;
	public function getWidth(): int;

	public function setBackgroundColor(ColorInterface $color): self;
	public function getBackgroundColor(): ColorInterface;

	public function setTextColor(ColorInterface $color): self;
	public function getTextColor(): ColorInterface;

	public function setTextShadowColor(ColorInterface $color): self;
	public function getTextShadowColor(): ColorInterface;

	public function setTextFont(string $font): self;
	public function getTextFont(): string;

	public function getTextSize(): int;
	public function setTextSize(int $size): self;

	public function getCacheEnabled(): bool;
	public function setCacheEnabled(bool $enable = true): self;

	public function setCacheDirectory(string $directory): self;
	public function getCacheDirectory(): string;
}