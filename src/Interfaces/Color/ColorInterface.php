<?php namespace SEStolk\ImagePlaceholder\Interfaces\Color;

interface ColorInterface {
	public function setRed(int $value);
	public function setGreen(int $value);
	public function setBlue(int $value);
	public function setAlpha(int $value);
}