<?php namespace SEStolk\ImagePlaceholder\Color;

use SEStolk\ImagePlaceholder\Interfaces\Color\ColorInterface;

class Color implements ColorInterface {
	/**
	 * @var int
	 */
	public $red = 0;
	/**
	 * @var int
	 */
	public $green = 0;
	/**
	 * @var int
	 */
	public $blue = 0;
	/**
	 * @var int
	 */
	public $alpha = 0;

	public function __construct(int $red = 0, int $green = 0, int $blue = 0, int $alpha = 0) {
		$this->red = $red;
		$this->green = $green;
		$this->blue = $blue;
		$this->alpha = $alpha;
	}

	/**
	 * @param int $value
	 * @return Color
	 */
	public function setRed(int $value): ColorInterface {
		$this->red = $value;

		return $this;
	}

	/**
	 * @param int $value
	 * @return Color
	 */
	public function setGreen(int $value): ColorInterface {
		$this->green = $value;

		return $this;
	}

	/**
	 * @param int $value
	 * @return Color
	 */
	public function setBlue(int $value): ColorInterface {
		$this->blue = $value;

		return $this;
	}

	/**
	 * @param int $value
	 * @return Color
	 */
	public function setAlpha(int $value): ColorInterface {
		$this->alpha = $value;

		return $this;
	}
}