<?php namespace SEStolk\ImagePlaceholder\Generator;

use SEStolk\ImagePlaceholder\Interfaces\Color\ColorInterface;
use SEStolk\ImagePlaceholder\Interfaces\ImageOptionsInterface;

class ImageOptions implements ImageOptionsInterface {

	/**
	 * @var int
	 */
	private $height;
	/**
	 * @var int
	 */
	private $width;
	/**
	 * @var ColorInterface
	 */
	private $backgroundColor;
	/**
	 * @var ColorInterface
	 */
	private $textColor;
	/**
	 * @var ColorInterface
	 */
	private $textShadowColor;
	/**
	 * @var bool
	 */
	private $cacheEnabled = true;
	/**
	 * @var string
	 */
	private $cacheDirectory;
	/**
	 * @var string
	 */
	private $textFont;
	/**
	 * @var int
	 */
	private $textSize;

	/**
	 * @param int $height
	 * @return ImageOptionsInterface
	 */
	public function setHeight(int $height): ImageOptionsInterface {
		$this->height = $height;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getHeight(): int {
		return $this->height;
	}

	/**
	 * @param int $width
	 * @return ImageOptionsInterface
	 */
	public function setWidth(int $width): ImageOptionsInterface {
		$this->width = $width;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getWidth(): int {
		return $this->width;
	}

	/**
	 *
	 * @param ColorInterface $color
	 * @return ImageOptions
	 */
	public function setBackgroundColor(ColorInterface $color): ImageOptionsInterface {
		$this->backgroundColor = $color;

		return $this;
	}

	/**
	 * @return ColorInterface
	 */
	public function getBackgroundColor(): ColorInterface {
		return $this->backgroundColor;
	}

	/**
	 *
	 * @param ColorInterface $color
	 * @return ImageOptions
	 */
	public function setTextColor(ColorInterface $color): ImageOptionsInterface {
		$this->textColor = $color;

		return $this;
	}

	/**
	 * @return ColorInterface
	 */
	public function getTextColor(): ColorInterface {
		return $this->textColor;
	}

	/**
	 *
	 * @param ColorInterface $color
	 * @return ImageOptions
	 */
	public function setTextShadowColor(ColorInterface $color): ImageOptionsInterface {
		$this->textShadowColor = $color;

		return $this;
	}

	/**
	 * @return ColorInterface
	 */
	public function getTextShadowColor(): ColorInterface {
		return $this->textShadowColor;
	}

	/**
	 * @return string
	 */
	public function getTextFont(): string {
		return $this->textFont ?? '/Library/Fonts/Arial.ttf';
	}

	/**
	 * @param string $font
	 * @return $this
	 */
	public function setTextFont(string $font): ImageOptionsInterface {
		$this->textFont = $font;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getTextSize(): int {
		return $this->textSize ?? $this->calculateTextSize();
	}

	/**
	 * @param int $size
	 * @return $this
	 */
	public function setTextSize(int $size): ImageOptionsInterface {
		$this->textSize = $size;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function getCacheEnabled(): bool {
		return $this->cacheEnabled;
	}

	/**
	 * @param bool $enable
	 * @return ImageOptionsInterface
	 */
	public function setCacheEnabled(bool $enable = true): ImageOptionsInterface {
		$this->cacheEnabled = $enable;

		return $this;
	}

	/**
	 * @param string $directory
	 * @return ImageOptions
	 */
	public function setCacheDirectory(string $directory): ImageOptionsInterface {
		$this->cacheDirectory = $directory;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getCacheDirectory(): string {
		return $this->cacheDirectory ?? storage_path('framework/cache');
	}

	/**
	 * @param int|null $width
	 * @param int|null $height
	 * @return int
	 */
	private function calculateTextSize(int $width = null, int $height = null): int {
		if (($width && $height) && ($width < 200 || $height < 200)) {
			return 20;
		}

		return 50;
	}
}