<?php namespace SEStolk\ImagePlaceholder\Generator;

use SEStolk\ImagePlaceholder\Interfaces\Color\ColorInterface;
use SEStolk\ImagePlaceholder\Interfaces\ImageOptionsInterface;

class ImagePlaceholder {
	/**
	 * @var ImageOptionsInterface
	 */
	private $imageOptions;
	/**
	 * @var string
	 */
	private $returnType = ImageReturnType::RETURN_TYPE_PATH;

	public function __construct(ImageOptionsInterface $imageOptions) {
		$this->imageOptions = $imageOptions;
	}

	/**
	 * @param resource $image
	 * @param ColorInterface $color
	 * @return int
	 */
	protected function getColor($image, ColorInterface $color): int {
		return imagecolorallocatealpha($image, $color->red, $color->green, $color->blue, $color->alpha);
	}

	/**
	 * @param string $filePath
	 * @return bool
	 */
	protected function isCached(string $filePath): bool {
		if ($this->imageOptions->getCacheEnabled() && is_file($filePath)) {
			return true;
		}

		return false;
	}

	/**
	 * @param string $filePath
	 * @return string|void
	 */
	protected function getResponse(string $filePath) {
		if ($this->returnType === ImageReturnType::RETURN_TYPE_NONE) {
			return;
		}

		// TODO As file response

		return $filePath;
	}

	/**
	 * @param string $returnType
	 * @return $this
	 */
	public function setReturnType(string $returnType): self {
		$this->returnType = $returnType;

		return $this;
	}

	/**
	 * @param string|null $imageText
	 * @return mixed
	 */
	public function generate(?string $imageText) {
		$height = $this->imageOptions->getHeight();
		$width = $this->imageOptions->getWidth();
		$imageText = $this->getImageText($imageText);

		$filePath = $this->imageOptions->getCacheDirectory() . '/' . $imageText . '.png';

		if ($this->isCached($filePath)) {
			return $this->getResponse($filePath);
		}

		$image = imagecreatetruecolor($width, $height);

		// Add background
		imagefilledrectangle($image, 0, 0, $width, $height, $this->getColor($image, $this->imageOptions->getBackgroundColor()));

		// Add text
		$this->addText($image, $imageText);

		// Stream PNG
		imagepng($image, $filePath);
		imagedestroy($image);

		if (is_file($filePath)) {
			return $this->getResponse($filePath);
		}
	}

	/**
	 * @param string $imageText
	 * @return array
	 */
	private function getTextPosition(string $imageText): array {
		[$lowerLeftX, $lowerLeftY, $lowerRightX, $lowerRightY] = imagettfbbox($this->imageOptions->getTextSize(), 0, $this->imageOptions->getTextFont(), $imageText);

		// Calculate center position
		$x = ceil(($this->imageOptions->getWidth() - ($lowerLeftX + $lowerRightX)) / 2);
		$y = ceil(($this->imageOptions->getHeight() - ($lowerLeftY + $lowerRightY)) / 2);

		return array($x, $y);
	}

	/**
	 * @param $image
	 * @param int $x
	 * @param int $y
	 * @param string $imageText
	 * @param ColorInterface $color
	 *
	 * @return array|bool
	 */
	protected function createText($image, int $x, int $y, string $imageText, ColorInterface $color) {
		return imagettftext(
			$image,
			$this->imageOptions->getTextSize(),
			0,
			$x,
			$y,
			$this->getColor(
				$image,
				$color
			),
			$this->imageOptions->getTextFont(),
			$imageText
		);
	}

	/**
	 * @param $image
	 * @param string $imageText
	 */
	protected function addText($image, string $imageText): void {
		[$x, $y] = $this->getTextPosition($imageText);

		// Add some shadow to the text
		$this->createText($image, $x + 1, $y + 1, $imageText, $this->imageOptions->getTextShadowColor());

		// Add text
		$this->createText($image, $x, $y, $imageText, $this->imageOptions->getTextColor());
	}

	/**
	 * @param string|null $imageText
	 * @return string
	 */
	protected function getImageText(?string $imageText): string {
		return $imageText ?? $this->imageOptions->getHeight() . 'x' . $this->imageOptions->getWidth();
	}
}