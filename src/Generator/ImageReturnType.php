<?php namespace SEStolk\ImagePlaceholder\Generator;

abstract class ImageReturnType {
	/**
	 * (Default) Use this as return type if you want only the file path to be returned
	 */
	public const RETURN_TYPE_PATH = 'path';
	/**
	 * Use this as return type if you just want the image to be created
	 */
	public const RETURN_TYPE_NONE = 'none';
}